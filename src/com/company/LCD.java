package com.company;

public class LCD {
    private String status = "Freeze";
    private int volume = 90;
    private int brightness = 15;
    private String cable = "VGA";

    public void turnOff() {
        status = "Off";
        System.out.println("-------------- LCD ----------------");
    }

    public void turnOn() {
        status = "On";
    }

    public void freeze() {
        status = "Freeze";
        System.out.println("Status LCD saat ini : " +status);
    }

    public void volumeUp() {
        volume++;
    }

    public void volumeDown() {
        volume--;
    }

    public void setVolume(int volume) {
        this.volume = volume;
        System.out.println("Volume saat ini : " + volume);
    }

    public void setBrightness(int brightness) {
        this.brightness = brightness;
        System.out.println("Brightness saat ini : " + brightness);
    }

    public void cableUp() {
        switch (cable) {
            case "VGA":
                cable = "DVI";
                break;
            case "DVI":
                cable = "HDMI";
                break;
            case "HDMI":
                cable = "Display Port";
                break;
            case "Display Port":
                cable = "VGA";
                break;
            default:
                cable = "VGA";
                break;
        }
    }

    public void cableDown() {
        switch (cable) {
            case "VGA":
                cable = "DVI";
                break;
            case "DVI":
                cable = "HDMI";
                break;
            case "HDMI":
                cable = "Display Port";
                break;
            case "Display Port":
                cable = "VGA";
                break;
            default:
                cable = "VGA";
                break;
        }
    }

    public void setCable(String cable) {
        this.cable = cable;
        cable = "VGA";

    }

    public void displayAll() {
        System.out.println("-------------- LCD ----------------");
        System.out.println("Status LCD saat ini    : " + status);
        System.out.println("Volume saat ini        : " + volume);
        System.out.println("Brightness saat ini    : " + brightness);
        System.out.println("Cable yang digunakanan : " + cable);
    }
}